<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            label,input,button{
                margin: 3px;
            }
        </style>
    </head>
    <body>
        <?php
        $numero=0;
        $salida='';
        ?>
        <form name="f">
            <label>Introduce un número de día:</label><input type="number" name="dia"><br>
            <button>Enviar</button>
        </form>
        <?php
        if($_REQUEST){
            $numero=$_REQUEST["dia"];
            switch($numero){
                case '1':
                    $salida="Lunes";
                    break;
                case '2':
                    $salida="Martes";
                    break;
                case '3':
                    $salida="Miércoles";
                    break;
                case '4':
                    $salida="Jueves";
                    break;
                case '5':
                    $salida="Viernes";
                    break;
                case '6':
                    $salida="Sábado";
                    break;
                case '7':
                    $salida="Domingo";
                    break;
                default:
                    $salida="Ese día no existe";
            }
            echo $salida;
        }
        ?>
    </body>
</html>
